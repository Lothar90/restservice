package pl.polsl.restservice;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.polsl.restservice.Director;
import pl.polsl.restservice.Position;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-10T19:31:40")
@StaticMetamodel(Film.class)
public class Film_ { 

    public static volatile SingularAttribute<Film, Director> directorId;
    public static volatile SingularAttribute<Film, Position> positionId;
    public static volatile SingularAttribute<Film, Integer> id;

}