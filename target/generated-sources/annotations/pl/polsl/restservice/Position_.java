package pl.polsl.restservice;

import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.polsl.restservice.Book;
import pl.polsl.restservice.Film;
import pl.polsl.restservice.Game;
import pl.polsl.restservice.Genre;
import pl.polsl.restservice.Rating;
import pl.polsl.restservice.Review;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-10T19:31:40")
@StaticMetamodel(Position.class)
public class Position_ { 

    public static volatile CollectionAttribute<Position, Genre> genreCollection;
    public static volatile SingularAttribute<Position, Double> averageRating;
    public static volatile CollectionAttribute<Position, Game> gameCollection;
    public static volatile CollectionAttribute<Position, Film> filmCollection;
    public static volatile SingularAttribute<Position, Short> accepted;
    public static volatile SingularAttribute<Position, Integer> id;
    public static volatile SingularAttribute<Position, String> title;
    public static volatile CollectionAttribute<Position, Book> bookCollection;
    public static volatile SingularAttribute<Position, BigInteger> premiere;
    public static volatile SingularAttribute<Position, String> graphic;
    public static volatile CollectionAttribute<Position, Review> reviewCollection;
    public static volatile CollectionAttribute<Position, Rating> ratingCollection;

}