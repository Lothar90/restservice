package pl.polsl.restservice;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.polsl.restservice.Film;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-10T19:31:40")
@StaticMetamodel(Director.class)
public class Director_ { 

    public static volatile CollectionAttribute<Director, Film> filmCollection;
    public static volatile SingularAttribute<Director, String> name;
    public static volatile SingularAttribute<Director, Integer> id;

}