package pl.polsl.restservice;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.polsl.restservice.Position;
import pl.polsl.restservice.User;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-10T19:31:40")
@StaticMetamodel(Review.class)
public class Review_ { 

    public static volatile SingularAttribute<Review, Position> positionId;
    public static volatile SingularAttribute<Review, String> review;
    public static volatile SingularAttribute<Review, Integer> id;
    public static volatile SingularAttribute<Review, User> userId;

}