package pl.polsl.restservice;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.polsl.restservice.Company;
import pl.polsl.restservice.Position;
import pl.polsl.restservice.Publisher;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-10T19:31:40")
@StaticMetamodel(Game.class)
public class Game_ { 

    public static volatile SingularAttribute<Game, Company> companyId;
    public static volatile SingularAttribute<Game, Publisher> publisherId;
    public static volatile SingularAttribute<Game, Position> positionId;
    public static volatile SingularAttribute<Game, Integer> id;

}