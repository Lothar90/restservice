package pl.polsl.restservice;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.polsl.restservice.Position;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-10T19:31:40")
@StaticMetamodel(Genre.class)
public class Genre_ { 

    public static volatile SingularAttribute<Genre, String> name;
    public static volatile CollectionAttribute<Genre, Position> positionCollection;
    public static volatile SingularAttribute<Genre, Integer> id;

}