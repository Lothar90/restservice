/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.restservice.service;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import pl.polsl.restservice.Position;
import pl.polsl.restservice.Rating;
import pl.polsl.restservice.Review;
import pl.polsl.restservice.model.ReviewData;

/**
 *
 * @author Rafał Swoboda
 */
@Stateless
@Path("position")
public class PositionFacadeREST extends AbstractFacade<Position> {

    @PersistenceContext(unitName = "pl.polsl_RESTService_war_1.0PU")
    private EntityManager em;

    public PositionFacadeREST() {
        super(Position.class);
    }

    @Override
    public void create(Position entity) {
        super.create(entity);
    }

    public void edit(@PathParam("id") Integer id, Position entity) {
        super.edit(entity);
    }

    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    public Position find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @POST
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public List<Position> findAll() {
        em.getEntityManagerFactory().getCache().evictAll();
        return super.findAll();
    }

    public List<Position> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @POST
    @Path("reviews/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ReviewData> getReviews(@PathParam("id") Integer positionId) {
        em.getEntityManagerFactory().getCache().evictAll();
        Position position = (Position) em.createNamedQuery("Position.findById")
                .setParameter("id", positionId).getSingleResult();
        List<Review> reviews = (List<Review>) position.getReviewCollection();
        List<Rating> ratings = (List<Rating>) position.getRatingCollection();
        List<ReviewData> positionReviews = new ArrayList<>();
        for (int i = 0; i < reviews.size(); i++) {
            ReviewData reviewData = new ReviewData();
            reviewData.setPositionId(reviews.get(i).getPositionId().getId());
            reviewData.setReview(reviews.get(i).getReview());
            reviewData.setUser(reviews.get(i).getUserId().getLogin());
            reviewData.setRating(ratings.get(i).getValue());
            positionReviews.add(reviewData);
        }
        return positionReviews;
    }

}
