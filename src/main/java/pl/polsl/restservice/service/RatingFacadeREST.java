/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.restservice.service;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import pl.polsl.restservice.Rating;

/**
 *
 * @author Rafał Swoboda
 */
@Stateless
@Path("rating")
public class RatingFacadeREST extends AbstractFacade<Rating> {

    @PersistenceContext(unitName = "pl.polsl_RESTService_war_1.0PU")
    private EntityManager em;

    public RatingFacadeREST() {
        super(Rating.class);
    }

    @Override
    public void create(Rating entity) {
        super.create(entity);
    }

    public void edit(@PathParam("id") Integer id, Rating entity) {
        super.edit(entity);
    }

    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    public Rating find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @POST
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public List<Rating> findAll() {
        em.getEntityManagerFactory().getCache().evictAll();
        return super.findAll();
    }

    public List<Rating> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
