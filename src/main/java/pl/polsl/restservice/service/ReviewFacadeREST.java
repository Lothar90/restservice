/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.restservice.service;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import pl.polsl.restservice.Position;
import pl.polsl.restservice.Rating;
import pl.polsl.restservice.Review;
import pl.polsl.restservice.User;
import pl.polsl.restservice.model.ReviewData;

/**
 *
 * @author Rafał Swoboda
 */
@Stateless
@Path("review")
public class ReviewFacadeREST extends AbstractFacade<Review> {

    @PersistenceContext(unitName = "pl.polsl_RESTService_war_1.0PU")
    private EntityManager em;

    public ReviewFacadeREST() {
        super(Review.class);
    }

    @Override
    public void create(Review entity) {
        super.create(entity);
    }

    public void edit(@PathParam("id") Integer id, Review entity) {
        super.edit(entity);
    }

    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    public Review find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @POST
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public List<Review> findAll() {
        em.getEntityManagerFactory().getCache().evictAll();
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Review> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @POST
    @Path("send")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response receiveReview(final ReviewData reviewData) {
        em.getEntityManagerFactory().getCache().evictAll();
        List<Review> reviews = em.createNamedQuery("Review.findAll").getResultList();
        List<Rating> ratings = em.createNamedQuery("Rating.findAll").getResultList();
        User user = (User) em.createNamedQuery("User.findByLogin")
                .setParameter("login", reviewData.getUser()).getSingleResult();
        Position position = (Position) em.createNamedQuery("Position.findById")
                .setParameter("id", reviewData.getPositionId()).getSingleResult();
        Review review = new Review();
        Rating rating = new Rating();
        review.setPositionId(position);
        rating.setPositionId(position);
        review.setReview(reviewData.getReview());
        rating.setValue(reviewData.getRating());
        review.setUserId(user);
        rating.setUserId(user);
        if (!reviews.contains(review) && !ratings.contains(rating)) {
            em.persist(review);
            em.persist(rating);
            List<Rating> positionRatings = (List<Rating>) position.getRatingCollection();
            double sum = 0;
            for (Rating r : positionRatings) {
                sum += r.getValue();
            }
            sum += rating.getValue();
            double avgRating = sum / (positionRatings.size() + 1);
            position.setAverageRating(avgRating);
            em.merge(position);
            return Response.ok(avgRating).build();
        } else {
            return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Ten tytuł był już recenzowany").build();
        }
    }

}
