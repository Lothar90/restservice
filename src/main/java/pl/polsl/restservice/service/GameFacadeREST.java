/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.restservice.service;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import pl.polsl.restservice.Game;

/**
 *
 * @author Rafał Swoboda
 */
@Stateless
@Path("game")
public class GameFacadeREST extends AbstractFacade<Game> {

    @PersistenceContext(unitName = "pl.polsl_RESTService_war_1.0PU")
    private EntityManager em;

    public GameFacadeREST() {
        super(Game.class);
    }

    @Override
    public void create(Game entity) {
        super.create(entity);
    }

    public void edit(@PathParam("id") Integer id, Game entity) {
        super.edit(entity);
    }

    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    public Game find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @POST
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public List<Game> findAll() {
        em.getEntityManagerFactory().getCache().evictAll();
        List<Game> allGames = em.createNamedQuery("Game.findAll").getResultList();
        List<Game> acceptedGames = new ArrayList<>();
        for (Game g : allGames) {
            if (g.getPositionId().getAccepted() == 1) {
                acceptedGames.add(g);
            }
        }
        return acceptedGames;
    }

    @POST
    @Path("proposition")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response proposition(Game game) {
        em.getEntityManagerFactory().getCache().evictAll();
        List<Game> games = em.createNamedQuery("Game.findAll").getResultList();
        if (games.contains(game)) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Taka pozycja już istnieje").build();
        }
        game.getPositionId().setAccepted((short) 0);
        game.getPositionId().setAverageRating(0.0);
        em.persist(game.getPositionId());
        em.persist(game);
        return Response.ok().build();
    }

    @POST
    @Path("image/{id}")
    public Response sendImage(@PathParam("id") Integer gameId) {
        try {
            em.getEntityManagerFactory().getCache().evictAll();
            Game game = (Game) em.createNamedQuery("Game.findById")
                    .setParameter("id", gameId).getSingleResult();
            if (game.getPositionId().getGraphic() == null) {
                return Response.status(Response.Status.NO_CONTENT).entity("Brak zdjęcia").build();
            }
            File file = new File(game.getPositionId().getGraphic());
            BufferedImage image = ImageIO.read(file);
            BufferedImage resizedImage = new BufferedImage(100, 140, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = resizedImage.createGraphics();
            g.drawImage(image, 0, 0, 100, 140, null);
            g.dispose();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(resizedImage, "png", baos);
            byte[] imageData = baos.toByteArray();
            return Response.ok(new ByteArrayInputStream(imageData)).build();
        } catch (IOException ex) {
            Logger.getLogger(GameFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    public List<Game> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
