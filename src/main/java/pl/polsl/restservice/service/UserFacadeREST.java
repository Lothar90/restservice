/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.restservice.service;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import pl.polsl.restservice.User;

/**
 *
 * @author Rafał Swoboda
 */
@Stateless
@Path("user")
public class UserFacadeREST extends AbstractFacade<User> {

    @PersistenceContext(unitName = "pl.polsl_RESTService_war_1.0PU")
    private EntityManager em;

    public UserFacadeREST() {
        super(User.class);
    }

    @Override
    public void create(User entity) {
        super.create(entity);
    }

    public void edit(@PathParam("id") Integer id, User entity) {
        super.edit(entity);
    }

    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    public User find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @Override
    public List<User> findAll() {
        return super.findAll();
    }

    public List<User> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @POST
    @Path("login")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(User user) {
        List<User> userList = em.createNamedQuery("User.findAll").getResultList();
        for (User u : userList) {
            if (user.getLogin().equals(u.getLogin())) {
                if (user.getPassword().equals(u.getPassword())) {
                    return Response.ok().build();
                } else {
                    return Response.status(Response.Status.UNAUTHORIZED).entity("Niepoprawne hasło").build();
                }
            }
        }
        return Response.status(Response.Status.NOT_FOUND).entity("Taki użytkownik nie istnieje").build();
    }

    @POST
    @Path("register")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response register(User user) {
        List<User> userList = em.createNamedQuery("User.findAll").getResultList();
        if(userList.contains(user))
            return Response.status(Response.Status.UNAUTHORIZED).entity("Użytkownik o takiej nazwie już istnieje").build();
        super.create(user);
        return Response.ok().build();
    }

    @POST
    @Path("logout")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response logout() {
        return Response.ok().build();
    }
    
}
