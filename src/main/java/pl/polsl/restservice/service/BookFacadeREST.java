/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.restservice.service;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import pl.polsl.restservice.Book;

/**
 *
 * @author Rafał Swoboda
 */
@Stateless
@Path("book")
public class BookFacadeREST extends AbstractFacade<Book> {

    @PersistenceContext(unitName = "pl.polsl_RESTService_war_1.0PU")
    private EntityManager em;

    public BookFacadeREST() {
        super(Book.class);
    }

    @Override
    public void create(Book entity) {
        super.create(entity);
    }

    public void edit(@PathParam("id") Integer id, Book entity) {
        super.edit(entity);
    }

    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    public Book find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @POST
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public List<Book> findAll() {
        em.getEntityManagerFactory().getCache().evictAll();
        List<Book> allBooks = em.createNamedQuery("Book.findAll").getResultList();
        List<Book> acceptedBooks = new ArrayList<>();
        for(Book b : allBooks) {
            if(b.getPositionId().getAccepted() == 1)
                acceptedBooks.add(b);
        }
        return acceptedBooks;
    }
    
    @POST
    @Path("proposition")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response proposition(Book book) {
        em.getEntityManagerFactory().getCache().evictAll();
        List<Book> books = em.createNamedQuery("Book.findAll").getResultList();
        if(books.contains(book))
            return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Taka pozycja już istnieje").build();
        book.getPositionId().setAccepted((short)0);
        book.getPositionId().setAverageRating(0.0);
        em.persist(book.getPositionId());
        em.persist(book);
        return Response.ok().build();
    }
    
    @POST
    @Path("image/{id}")
    public Response sendImage(@PathParam("id") Integer bookId) {
        try {
            em.getEntityManagerFactory().getCache().evictAll();
            Book book = (Book) em.createNamedQuery("Book.findById")
                    .setParameter("id", bookId).getSingleResult();
            if (book.getPositionId().getGraphic() == null) {
                return Response.status(Response.Status.NO_CONTENT).entity("Brak zdjęcia").build();
            }
            File file = new File(book.getPositionId().getGraphic());
            BufferedImage image = ImageIO.read(file);
            BufferedImage resizedImage = new BufferedImage(100, 140, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = resizedImage.createGraphics();
            g.drawImage(image, 0, 0, 100, 140, null);
            g.dispose();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(resizedImage, "png", baos);
            byte[] imageData = baos.toByteArray();
            return Response.ok(new ByteArrayInputStream(imageData)).build();
        } catch (IOException ex) {
            Logger.getLogger(GameFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    public List<Book> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
