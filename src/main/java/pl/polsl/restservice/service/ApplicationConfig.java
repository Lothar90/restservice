/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.restservice.service;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Rafał Swoboda
 */
@javax.ws.rs.ApplicationPath("res")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(pl.polsl.restservice.service.AuthorFacadeREST.class);
        resources.add(pl.polsl.restservice.service.BookFacadeREST.class);
        resources.add(pl.polsl.restservice.service.CompanyFacadeREST.class);
        resources.add(pl.polsl.restservice.service.DirectorFacadeREST.class);
        resources.add(pl.polsl.restservice.service.FilmFacadeREST.class);
        resources.add(pl.polsl.restservice.service.GameFacadeREST.class);
        resources.add(pl.polsl.restservice.service.GenreFacadeREST.class);
        resources.add(pl.polsl.restservice.service.PositionFacadeREST.class);
        resources.add(pl.polsl.restservice.service.PublisherFacadeREST.class);
        resources.add(pl.polsl.restservice.service.RatingFacadeREST.class);
        resources.add(pl.polsl.restservice.service.ReviewFacadeREST.class);
        resources.add(pl.polsl.restservice.service.UserFacadeREST.class);
    }
    
}
