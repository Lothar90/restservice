/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.restservice.model;

/**
 *
 * @author Rafał Swoboda
 */
public class ReviewData {
    
    String user;
    String review;
    double rating;
    int positionId;

    public ReviewData() {
    }

    public ReviewData(String user, String review, double rating, int positionId) {
        this.user = user;
        this.review = review;
        this.rating = rating;
        this.positionId = positionId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }
    
}
